package com.akhil.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaOracleDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaOracleDemoApplication.class, args);
	}

}

